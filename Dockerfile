# FROM hub.haifengat.com/haifengat/oracle_base:19
# 最小化镜像
FROM hub.haifengat.com/library/busybox:glibc
ARG AppName
WORKDIR /app
COPY ./Shanghai /usr/share/zoneinfo/Asia/
RUN ln -fs /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
ENV TZ=Asia/Shanghai

COPY ./${AppName} ./

# 基础依赖库
# COPY lib64 /lib64
# ENV LD_LIBRARY_PATH /app:\$LD_LIBRARY_PATH
ENTRYPOINT ["./portainer_manager"]
